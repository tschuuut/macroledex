#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#define PIN        6              // LED Data pin
#define NUMPIXELS  4              // Number of leds
#define ANIMATION_LENGTH 30000    // How long one animation is played (in milliseconds)

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

uint32_t possible_colors[3];
uint32_t getRandomColor() {
  int possible_colors_length = sizeof(possible_colors) / sizeof(possible_colors[0]);
  return possible_colors[random(possible_colors_length)];
}

class Animations {
  public:
    class Animation;
    class Running;
    class Blinking;
    class Switching;
    class Random;
    static Animations::Animation* getRandomAnimation();
};

class Animations::Animation {
  public:
    virtual void setup(uint32_t color) = 0;
    virtual void loop() = 0;
  protected:
    uint32_t color = 0;
};

class Animations::Running : public Animations::Animation {
  public:
    void setup(uint32_t color);
    void loop();

  private:
    int step = 0;
    int step_length = 0;
};

void Animations::Running::setup(uint32_t color) {
  this->step = 0;
  this->color = color;
  this->step_length = 500;
};
void Animations::Running::loop() {
  pixels.setPixelColor(this->step, 0, 0, 0);
  this->step = (this->step + 1) % NUMPIXELS;
  pixels.setPixelColor(this->step, this->color);
  delay(this->step_length);
};

class Animations::Blinking : public Animations::Animation {
  public:
    void setup(uint32_t color);
    void loop();

  private:
    bool on;
    int step_length = 0;
};

void Animations::Blinking::setup(uint32_t color) {
  this->color = color;
  this->on = true;
  this->step_length = 500;
};

void Animations::Blinking::loop() {
  pixels.fill(this->color * this->on);
  this->on = !this->on;
  delay(this->step_length);
};

class Animations::Switching : public Animations::Animation {
  public:
    void setup(uint32_t color);
    void loop();
  private:
    bool step;
    int step_length;
};

void Animations::Switching::setup(uint32_t color) {
  this->color = color;
  this->step = 0;
  this->step_length = 500;
};

void Animations::Switching::loop() {
  for(int i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, this->color * ((i + step) % 2));
  }
  this->step = (this->step + 1) % 2;
  delay(this->step_length);
};

class Animations::Random : public Animations::Animation {
  public:
    void setup(uint32_t color);
    void loop();
  private:
    int step_length;
};

void Animations::Random::setup(uint32_t color) {
  this->step_length = 1000;
  this->color = color;
};

void Animations::Random::loop() {
  for(int i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, this->color * random(2));
  }
  delay(this->step_length);
};

Animations::Animation* animations[4];

Animations::Animation* Animations::getRandomAnimation() {
  int animations_length = sizeof(animations) / sizeof(animations[0]);
  return animations[random(animations_length)];
}

Animations::Animation* animation;

void setup() {
  randomSeed(analogRead(0));
  animations[0] = new Animations::Running();
  animations[1] = new Animations::Blinking();
  animations[2] = new Animations::Switching();
  animations[3] = new Animations::Random();

  possible_colors[0] = pixels.Color(255, 255,   0);
  possible_colors[1] = pixels.Color(180,   0,   0);
  possible_colors[2] = pixels.Color( 40,  40, 255);

  pixels.begin();
}

void loop() {
  unsigned long next_switch = millis() + ANIMATION_LENGTH;

  animation = Animations::getRandomAnimation();
  uint32_t color = getRandomColor();
  animation->setup(color);

  while(millis() < next_switch) {
    animation->loop();
    pixels.show();
  }
}

